﻿using System;
using System.Collections.Generic;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage
{
	public interface ICarriage
	{
		CarriageType GetCarriageType();
		decimal GetCurrentWeight();
		decimal GetMaxWeight();
		IEnumerator<ICargo> GetEnumerator();
	}
}
