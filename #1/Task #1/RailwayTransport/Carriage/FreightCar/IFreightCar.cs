﻿using System;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.FreightCar
{
	public interface IFreightCar
	{
		void AddLoad(Freight load);
		void RemoveLoad(Freight load);
	}
}
