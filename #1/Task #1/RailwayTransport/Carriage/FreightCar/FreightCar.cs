﻿using System;
using System.Collections.Generic;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.FreightCar
{
	public class FreightCar : Carriage, IFreightCar
	{
		private readonly ICollection<Freight> Goods;
		private readonly decimal MaxVolume;
		
		public FreightCar(decimal maxWeight, decimal maxVolume) : base(CarriageType.FreightCar, maxWeight)
		{
			Goods = new List<Freight>();
			MaxVolume = maxVolume;
		}

		public void AddLoad(Freight load)
		{
			if (load.GetVolume() > MaxVolume)
				throw new OverflowException();
			if (load.GetWeight() > MaxWeight)
				throw new OverflowException();
			Goods.Add(load);
		}

		public void RemoveLoad(Freight load)
		{
			Goods.Remove(load);
		}
		
		public override decimal GetCurrentWeight()
		{
			decimal result = 0;
			foreach(var item in Goods)
			{
				result += item.GetWeight();
			}
			return result;
		}
		
		public override IEnumerator<ICargo> GetEnumerator()
		{
			return Goods.GetEnumerator();
		}
	}
}
