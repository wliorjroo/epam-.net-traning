﻿using System;

namespace RailwayTransport.Carriage
{
	public enum CarriageType
	{
		Locomotive,
		PassengerCar,
		FreightCar
	}
}
