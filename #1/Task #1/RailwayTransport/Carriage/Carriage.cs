﻿using System;
using System.Collections.Generic;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage
{
	public abstract class Carriage : ICarriage
	{
		protected readonly CarriageType Type;
		protected readonly decimal MaxWeight;
		protected decimal CurrentLoad { get; set; }
		
		protected Carriage(CarriageType type) : this(type, 0, 0)
		{
		}
		
		protected Carriage(CarriageType type, decimal maxWeight) : this(type, maxWeight, 0)
		{
		}
		
		protected Carriage(CarriageType type, decimal maxWeight, decimal currentLoad)
		{
			Type = type;
			MaxWeight = maxWeight;
			CurrentLoad = currentLoad;
		}

		CarriageType ICarriage.GetCarriageType()
		{
			return Type;
		}

		public virtual decimal GetCurrentWeight()
		{
			return CurrentLoad;
		}

		public virtual decimal GetMaxWeight()
		{
			return MaxWeight;
		}

		public abstract IEnumerator<ICargo> GetEnumerator();
	}
}
