﻿using System;

namespace RailwayTransport.Carriage.PassengerCar
{
	public enum ResidentialClass
	{
		UltraEconom,
		Econom,
		Coupe
	}
}
