﻿using System;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.PassengerCar
{
	public class DiningCar : PassengerCar
	{
		public DiningCar(decimal maxWeight, int passengersMaxCount) : base(PassengerCarType.Restaurant, maxWeight, passengersMaxCount)
		{
		}

		public override void AddPassenger(Passenger passenger)
		{	
			if (Passengers.Count >= PassengersMaxCount)
			{
				throw new OverflowException("there are no empty seats");
			}	
			Passengers.Add(passenger);
		}
		
		// TODO Realization of DiningCar
	}
}
