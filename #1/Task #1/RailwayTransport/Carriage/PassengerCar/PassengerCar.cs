﻿using System;
using System.Collections.Generic;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.PassengerCar
{
	public abstract class PassengerCar : Carriage, IPassengerCar
	{
		public int MaxPassengers
		{
			get {
				return PassengersMaxCount;
			}
		}
		
		protected readonly PassengerCarType PassengerCarType;
		protected readonly int PassengersMaxCount;
		
		public ICollection<Passenger> Passengers { get; protected set; }
		
		protected PassengerCar(PassengerCarType carType, decimal maxWeight, int passengersMaxCount) : this(CarriageType.PassengerCar, carType, maxWeight, passengersMaxCount)
		{
		}
		
		protected PassengerCar(CarriageType type, PassengerCarType carType, decimal maxWeight, int passengersMaxCount) : base(type, maxWeight)
		{
			PassengerCarType = carType;
			PassengersMaxCount = passengersMaxCount;
			Passengers = new List<Passenger>(passengersMaxCount);
		}
		
		public abstract void AddPassenger(Passenger passenger);
		
		public void RemovePassenger(Passenger passenger)
		{
			Passengers.Remove(passenger);
		}

		public PassengerCarType GetPassengerCarType()
		{
			return PassengerCarType;
		}
		
		public override IEnumerator<ICargo> GetEnumerator()
		{
			return Passengers.GetEnumerator();
		}
	}
}
