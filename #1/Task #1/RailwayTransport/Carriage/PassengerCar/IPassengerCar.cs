﻿using System;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.PassengerCar
{
	public interface IPassengerCar : ICarriage
	{
		int MaxPassengers { get; }
		void AddPassenger(Passenger passenger);
		void RemovePassenger(Passenger passenger);
		PassengerCarType GetPassengerCarType();
	}
}
