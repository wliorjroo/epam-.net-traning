﻿using System;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.PassengerCar
{
	public class ResidentialCar : PassengerCar
	{
		protected readonly ResidentialClass Class;
		protected decimal BaggageMax 
		{
			get {
				return MaxWeight / PassengersMaxCount;
			}
		}
		
		public ResidentialCar(ResidentialClass carClass, decimal maxWeight, int passengersMaxCount) : base(PassengerCarType.Residential, maxWeight, passengersMaxCount)
		{
			Class = carClass;
		}
		
		public override void AddPassenger(Passenger passenger)
		{		
			if (Passengers.Count >= PassengersMaxCount)
			{
				throw new OverflowException("there are no empty seats");
			}
			if (passenger.GetWeight() > BaggageMax)
			{
				throw new OverflowException("passenger has too much baggage");
			}
			Passengers.Add(passenger);
		}
		
		public ResidentialClass GetClass()
		{
			return Class;
		}
		
		public override decimal GetCurrentWeight()
		{
			decimal result = 0;
			foreach(var item in Passengers)
				result += item.GetWeight();
			return result;
		}
	}
}
