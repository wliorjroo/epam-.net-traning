﻿using System;

namespace RailwayTransport.Carriage.Locomotive
{
	public class ElectricLocomotive : RailwayLocomotive
	{
		public ElectricLocomotive(decimal power) : base(power, EngineType.Electric)
		{
		}
		
		// TODO Realization of ElectricLocomotive
	}
}
