﻿using System;

namespace RailwayTransport.Carriage.Locomotive
{
	public interface ILocomotive
	{
		decimal GetPower();
		EngineType GetEngineType();
	}
}
