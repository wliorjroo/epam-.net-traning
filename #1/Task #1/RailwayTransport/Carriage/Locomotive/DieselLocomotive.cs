﻿using System;

namespace RailwayTransport.Carriage.Locomotive
{
	public class DieselLocomotive : RailwayLocomotive
	{		
		public DieselLocomotive(decimal power) : base(power, EngineType.Diesel)
		{
		}
		
		// TODO Realization of DieselLocomotive
	}
}
