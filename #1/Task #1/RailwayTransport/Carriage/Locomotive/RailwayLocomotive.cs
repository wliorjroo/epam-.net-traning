﻿using System;
using System.Collections.Generic;
using RailwayTransport.Cargo;

namespace RailwayTransport.Carriage.Locomotive
{
	public abstract class RailwayLocomotive : Carriage, ILocomotive
	{
		protected decimal Power;
		protected EngineType Engine;
		
		protected RailwayLocomotive(EngineType engine) : base(CarriageType.Locomotive)
		{
			Engine = engine;
		}
		
		protected RailwayLocomotive(decimal power, EngineType engine) : this(engine)
		{
			Power = power;
		}

		public decimal GetPower()
		{
			return Power;
		}

		public EngineType GetEngineType()
		{
			return Engine;
		}
		
		public override IEnumerator<ICargo> GetEnumerator()
		{
			throw new NotImplementedException();
		}
	}
}
