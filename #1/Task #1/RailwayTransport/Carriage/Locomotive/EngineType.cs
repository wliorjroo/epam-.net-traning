﻿using System;

namespace RailwayTransport.Carriage.Locomotive
{
	public enum EngineType
	{
		Diesel,
		Electric
	}
}
