﻿using System;

namespace RailwayTransport.Cargo
{
	public static class Sequence
	{
		private static uint _sequence = 0;
		public static uint Next
		{
			get {
				return ++_sequence;
			}
		}
	}
}
