﻿using System;

namespace RailwayTransport.Cargo
{
	public abstract class Cargo : ICargo
	{
		protected readonly uint Id;
		
		protected Cargo()
		{
			Id = Sequence.Next;
		}
		
		public abstract decimal GetWeight();
		
		public uint GetId()
		{
			return Id;
		}
	}
}
