﻿using System;

namespace RailwayTransport.Cargo
{
	public enum FreightType
	{
		Dry,
		Liquid,
		Gas
	}
}
