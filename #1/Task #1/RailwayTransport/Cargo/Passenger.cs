﻿using System;

namespace RailwayTransport.Cargo
{
	public class Passenger : Cargo
	{
		private readonly decimal Baggage = 0;
		
		public Passenger(decimal baggage)
		{
			Baggage = baggage;
		}
		
		public override decimal GetWeight()
		{
			return Baggage;
		}
	}
}
