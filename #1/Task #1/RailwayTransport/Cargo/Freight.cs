﻿using System;

namespace RailwayTransport.Cargo
{
	public class Freight : Cargo
	{
		public readonly FreightType Type;
		
		private readonly decimal Weight;
		private readonly decimal Volume;
		
		public Freight(FreightType type, decimal weight, decimal volume)
		{
			Type = type;
			Weight = weight;
			Volume = volume;
		}

		public override decimal GetWeight()
		{
			return Weight;
		}
		
		public decimal GetVolume()
		{
			return Volume;
		}
	}
}
