﻿using System;

namespace RailwayTransport.Cargo
{
	public interface ICargo
	{
		uint GetId();
		decimal GetWeight();
	}
}
