﻿using System;
using System.Collections.Generic;
using RailwayTransport.Cargo;
using RailwayTransport.Carriage;

namespace RailwayTransport.Train
{
	public interface ITrain: ICollection<ICarriage>
	{
		decimal GetWeight();
		void Sort(Comparison<ICarriage> comparison);
		void AddPassengers(int[] passengerCount, decimal[] averageBaggage);
		IEnumerable<ICargo> GetCargo();
		IEnumerable<ICarriage> FindAll(Predicate<ICarriage> match);
	}
}
