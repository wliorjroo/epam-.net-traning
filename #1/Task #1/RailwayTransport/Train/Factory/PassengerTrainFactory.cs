﻿using System;
using RailwayTransport.Carriage;
using RailwayTransport.Carriage.Locomotive;
using RailwayTransport.Carriage.PassengerCar;
using RailwayTransport.Train.Factory.RailwayDepot;

namespace RailwayTransport.Train.Factory
{
	public class PassengerTrainFactory : IFactory 
	{
		private PassengerCarType CarType;
		private ResidentialClass CarClass;
		private int PassengersCount;
        private EngineType Type = EngineType.Diesel;

        public PassengerTrainFactory(PassengerCarType carType, int passangersCount, ResidentialClass carClass)
		{
			if (passangersCount <= 0)
				throw new ArgumentException();
			CarType = carType;
			CarClass = carClass;
			PassengersCount = passangersCount;
		}

		public ITrain Create()
		{
			bool flag = true;
			int current = 0;
			ITrain result = new ConcreteTrain();
			while(flag)
			{
				var carriage = PassengerCarDepot.GetCar(CarType, CarClass);
				result.Add(carriage);
				if ((current += carriage.MaxPassengers) >= PassengersCount)
				    flag = false;
			}
			
			var weight = result.GetWeight();
			flag = true;
			decimal currentPower = 0;
			while(flag)
			{
				var locomotive = LocomotiveDepot.GetLocomotive(Type);
				result.Add(locomotive);
				if ((currentPower += locomotive.GetPower()) >= weight)
				{
					flag = false;
				}
			}
			
			return result;
		}
	}
}
