﻿using System;

namespace RailwayTransport.Train.Factory
{
	public interface IFactory
	{
		ITrain Create();
	}
}
