﻿using System;
using RailwayTransport.Carriage.Locomotive;

namespace RailwayTransport.Train.Factory.RailwayDepot
{
	public static class LocomotiveDepot
	{
		private const decimal dieselPower = 1000000;
		private const decimal electricPower = 500000;
		
		public static RailwayLocomotive GetLocomotive(EngineType engine)
		{
			switch(engine)
			{
				case EngineType.Diesel:
					return new DieselLocomotive(dieselPower);
				case EngineType.Electric:
					return new ElectricLocomotive(electricPower);
				default:
					break;
			}
			return null;
		}
	}
}
