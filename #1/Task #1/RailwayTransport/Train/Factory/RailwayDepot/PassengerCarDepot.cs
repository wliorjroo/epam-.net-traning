﻿using System;
using RailwayTransport.Carriage.PassengerCar;

namespace RailwayTransport.Train.Factory.RailwayDepot
{
	public static class PassengerCarDepot
	{		
		private const decimal MaxWeight = 60000;
		
		public static PassengerCar GetCar(ResidentialClass carClass)
		{
			int maxPassengersCount = 0;
			switch(carClass)
			{
				case ResidentialClass.Coupe:
					maxPassengersCount = 60;
					break;
				case ResidentialClass.Econom:
					maxPassengersCount = 80;
					break;
				default:
					maxPassengersCount = 120;
					break;
			}
			return new ResidentialCar(carClass, MaxWeight, maxPassengersCount);
		}
		
		public static PassengerCar GetCar(PassengerCarType type, ResidentialClass carClass)
		{
			switch (type)
			{
				case  PassengerCarType.Restaurant:
					return new DiningCar(MaxWeight, 40);
				default:
					return GetCar(carClass);
			}
			
		}
	}
}
