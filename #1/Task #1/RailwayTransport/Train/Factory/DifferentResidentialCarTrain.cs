﻿using RailwayTransport.Carriage.Locomotive;
using RailwayTransport.Carriage.PassengerCar;
using RailwayTransport.Train.Factory.RailwayDepot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailwayTransport.Train.Factory
{
    public class DifferentResidentialCarTrain : IFactory
    {
        private PassengerCarType CarType;
        private int[] PassengersCount;
        private EngineType Type = EngineType.Diesel;

        public DifferentResidentialCarTrain(int[] passengersCount)
        {
            if (passengersCount.Length != Enum.GetNames(typeof(ResidentialClass)).Length)
                throw new ArgumentException();
            CarType = PassengerCarType.Residential;
            PassengersCount = passengersCount;
        }

        public ITrain Create()
        {
            ITrain result = new ConcreteTrain();
            for (int i = 0; i < PassengersCount.Length; i++)
            {
            	if (PassengersCount[i] > 0)
            	{
	                ResidentialClass carClass = (ResidentialClass)Enum.Parse(typeof(ResidentialClass), i.ToString());
	                int current = 0;
	                PassengerCar carriage;
	                do
	                {
	                    carriage = PassengerCarDepot.GetCar(CarType, carClass);
	                    result.Add(carriage);
	                }
	                while((current += carriage.MaxPassengers) >= PassengersCount[i]);
            	}
            }

            var weight = result.GetWeight();
            decimal currentPower = 0;
            RailwayLocomotive locomotive;
            do
            {
                locomotive = LocomotiveDepot.GetLocomotive(Type);
                result.Add(locomotive);
            }
            while ((currentPower += locomotive.GetPower()) >= weight);

            return result;
        }
    }
}
