﻿using System;
using System.Collections;
using System.Collections.Generic;
using RailwayTransport.Cargo;
using RailwayTransport.Carriage;
using RailwayTransport.Carriage.PassengerCar;

namespace RailwayTransport.Train
{
	public class ConcreteTrain : ITrain
	{
		readonly List<ICarriage> Carriages;
		
		public ConcreteTrain()
		{
			Carriages = new List<ICarriage>();
		}

		public decimal GetWeight()
		{
			decimal result = 0;
			foreach(var item in Carriages)
			{
				result += item.GetCurrentWeight();
			}
			return 0;
		}
		
		public void Sort(Comparison<ICarriage> comparison)
		{
			Carriages.Sort(comparison);
		}
		
		/// <summary>
		/// метод для добавления пассажиров в вагоны по классу комфортности
		/// </summary>
		/// <param name="passengerCount"> массив кол-ва пассажиров для конкретного класса </param>
		/// <param name="averageBaggage"> кол-во багажа для каждого пассажира </param>
		public void AddPassengers(int[] passengerCount, decimal[] averageBaggage)
		{
			List<ResidentialCar>[] carriages = new List<ResidentialCar>[Enum.GetNames(typeof(ResidentialClass)).Length];
			foreach(var item in Carriages.FindAll(x => x is ResidentialCar))
			{
				var car = (ResidentialCar)item;
				int enumValue = (int)car.GetClass();
				if (carriages[enumValue] == null)
					carriages[enumValue] = new List<ResidentialCar>();
				carriages[enumValue].Add(car);
			}
			
			for(int i = 0; i < passengerCount.Length; i++)
			{
				if (passengerCount[i] > 0)
				{
					var current = passengerCount[i];
					foreach(var item in carriages[i])
					{
						var maxPasssengers = item.MaxPassengers;
						for (int j = 0; j < item.MaxPassengers && current > 0; j++, current--)
						{
							item.AddPassenger(new Passenger(averageBaggage[i]));
						}
					}
				}
			}
		}
		
		public IEnumerable<ICargo> GetCargo()
		{
			foreach(var car in Carriages)
			{
				if (car.GetCarriageType() != CarriageType.Locomotive)
				{
					foreach(var cargo in car)
						yield return cargo;
				}
			}
		}
		
		public IEnumerable<ICarriage> FindAll(Predicate<ICarriage> match)
		{
			foreach(var car in Carriages)
			{
				if (match.Invoke(car))
					yield return car;
			}
		}

		#region ICollection implementation

		public void Add(ICarriage item)
		{
			Carriages.Add(item);
		}
		
		public void Clear()
		{
			Carriages.Clear();
		}
		
		public bool Contains(ICarriage item)
		{
			return Carriages.Contains(item);
		}
		
		public void CopyTo(ICarriage[] array, int arrayIndex)
		{
			Carriages.CopyTo(array, arrayIndex);
		}
		
		public bool Remove(ICarriage item)
		{
			return Carriages.Remove(item);
		}

		public int IndexOf(ICarriage item)
		{
			// TODO Realization of IndexOf
			throw new NotImplementedException();
		}
		public void Insert(int index, ICarriage item)
		{
			// TODO Realization of Insert
			throw new NotImplementedException();
		}
		public void RemoveAt(int index)
		{
			// TODO Realization of RemoveAt
			throw new NotImplementedException();
		}
		public ICarriage this[int index] {
			// TODO Realization of this[int index]
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}		
		public int Count {
			get {
				return Carriages.Count;
			}
		}
		
		public bool IsReadOnly {
			// TODO Realization of IsReadOnly
			get {
				throw new NotImplementedException();
			}
		}
		
		#endregion
		
		#region IEnumerable implementation
		
		public IEnumerator<ICarriage> GetEnumerator()
		{
			return Carriages.GetEnumerator();
		}
		
		#endregion
		
		#region IEnumerable implementation
		
		IEnumerator IEnumerable.GetEnumerator()
		{
			return Carriages.GetEnumerator();
		}
		
		#endregion
	}
}
