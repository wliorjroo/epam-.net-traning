﻿using System;
using System.Collections.Generic;
using RailwayTransport.Carriage;
using RailwayTransport.Carriage.PassengerCar;

namespace ConsoleUI
{
	public abstract class ScenarioCommand
	{
		protected string wrongInput = "произошла ошибка повторите ввод";
		
		protected ScenarioCommand()
		{
		}
		
		protected virtual void WriteLine(string line)
		{
			Console.WriteLine(line);
		}
		
		protected virtual void WriteLines(string[] lines)
		{
			foreach(var line in lines)
			{
				Console.WriteLine(line);
			}
		}
		
		protected virtual string[] Read(int splitCount)
		{
			return Console.ReadLine().Split(new char[] {' '}, splitCount);
		}
		
		protected void WriteCarriages(IEnumerable<ICarriage> train)
		{
			string message = "Вагоны:";
			string outputFormat = "тип вагона: {0} , кол-во пасажиров: {1} , вес багажа: {2}";
			WriteLine(message);
			foreach(var car in train)
			{
				int passengerCount = 0;
				string carriageType = car.GetCarriageType().ToString();
				if (car.GetCarriageType() == CarriageType.PassengerCar)
				{
					var passengerCar = (car as PassengerCar);
					passengerCount = passengerCar.Passengers.Count;
					carriageType += passengerCar.GetPassengerCarType().ToString();
					if (passengerCar.GetPassengerCarType() == PassengerCarType.Residential)
					{
						var residentialCar = (ResidentialCar)passengerCar;
						carriageType += residentialCar.GetClass().ToString();
					}
				}
				WriteLine(string.Format(outputFormat, carriageType, passengerCount, car.GetCurrentWeight()));
			}
		}
		
		public abstract void Execute();
	}
}
