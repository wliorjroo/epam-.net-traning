﻿using System;
using System.Collections;
using System.Collections.Generic;
using RailwayTransport.Cargo;
using RailwayTransport.Carriage;
using RailwayTransport.Carriage.Locomotive;
using RailwayTransport.Carriage.PassengerCar;
using RailwayTransport.Train;
using RailwayTransport.Train.Factory;

namespace ConsoleUI
{
	public class ScenarioCreate : ScenarioCommand
	{
		// TODO refactoring
		
		string[] lines = new string[]
		{
			"создание поезда:",
			"введите уровень комфортности вагона и кол-во пассажиров",
			"имитация добавления пассажиров:",
			"введите уровень комфортности вагона кол-во добавляемых пассажиров и среднее кол-во багажа на каждого",
			"кол-во пасажиров: {0} , вес багажа: {1}",
			"Вагоны:",
			"тип вагона: {0} , кол-во пасажиров: {1} , вес багажа: {2}",
			"введите диапозон для поиска вагонов"
		};
		
		string wrongInput = "произошла ошибка повторите ввод";
		
		public ScenarioCreate()
		{
		}
		
		public override void Execute()
		{
			ITrain train = CreateTrain();
			AddPassengers(train);
			CountPassengers(train);
			Sort(train);
			Find(train);
			Console.ReadKey();
		}
		
		private ITrain CreateTrain()
		{
			//WriteLine(lines[0]);
            WriteLine(lines[1]);
            int[] passengersCount = new int[Enum.GetNames(typeof(ResidentialClass)).Length];
			while(true)
			{
				try
				{
					// TODO validation
					var readed = Read(2);
					
					if (readed.Length == 2)
					{
						ResidentialClass carClass;
						Enum.TryParse<ResidentialClass>(readed[0], false, out carClass);
						int count = int.Parse(readed[1]);
						
						passengersCount[(int)carClass] += count;
					}
					else
					{
						return new DifferentResidentialCarTrain(passengersCount).Create();
					}
				}
				catch (Exception ex)
				{
					WriteLines(new string[] { "error:", ex.Message });
				}
			}
		}
		
		private void AddPassengers(ITrain train)
		{
			//WriteLine(lines[2]);
            WriteLine(lines[3]);
            int[] passengersCount = new int[Enum.GetNames(typeof(ResidentialClass)).Length];
			decimal[] averageBaggage = new decimal[passengersCount.Length];
			while(true)
			{
				try
				{
					// TODO validation
					var readed = Read(3);
					if (readed.Length == 3)
					{						
						ResidentialClass carClass;
						Enum.TryParse<ResidentialClass>(readed[0], out carClass);
						int count = int.Parse(readed[1]);
						decimal baggage = decimal.Parse(readed[2]);
						
						passengersCount[(int)carClass] += count;
						if (baggage > averageBaggage[(int)carClass])
						{
							averageBaggage[(int)carClass] = baggage;
						}
					}
					else
					{						
						train.AddPassengers(passengersCount, averageBaggage);
						break;
					}
				}
				catch (Exception ex)
				{
					WriteLines(new string[] { "error:", ex.Message });
				}
			}
		}
		
		private void CountPassengers(ITrain train)
		{
			int count = 0;
			decimal weight = 0;
			foreach(var cargo in train.GetCargo())
			{
				var passenger = (cargo as Passenger);
				weight += cargo.GetWeight();
				count++;
			}
			
			WriteLine(string.Format(lines[4], count, weight));
		}
		
		private void Sort(ITrain train)
		{
			WriteCarriages(train);
			train.Sort(Sort);
			WriteCarriages(train);
		}
		
		private void WriteCarriages(IEnumerable<ICarriage> train)
		{
			//WriteLine(lines[5]);
			foreach(var car in train)
			{
				int passengerCount = 0;
				string carriageType = car.GetCarriageType().ToString();
				if (car.GetCarriageType() == CarriageType.PassengerCar)
				{
					var passengerCar = (car as PassengerCar);
					passengerCount = passengerCar.Passengers.Count;
					carriageType += passengerCar.GetPassengerCarType().ToString();
					if (passengerCar.GetPassengerCarType() == PassengerCarType.Residential)
					{
						var residentialCar = (ResidentialCar)passengerCar;
						carriageType += residentialCar.GetClass().ToString();
					}
				}
				WriteLine(string.Format(lines[6], carriageType, passengerCount, car.GetCurrentWeight()));
			}
		}
		
		private int Sort(ICarriage carriage1, ICarriage carriage2)
		{
			if (carriage1.GetCarriageType() == CarriageType.Locomotive)
			{
				if (carriage2.GetCarriageType() == CarriageType.Locomotive)
					return 0;
				else
					return -1;
			}
			if (carriage1.GetCarriageType() == CarriageType.PassengerCar)
			{
				if (carriage2.GetCarriageType() == CarriageType.PassengerCar)
					return Comparer.Default.Compare((int)carriage1.GetCarriageType(), (int)carriage2.GetCarriageType());
				else
					return 1;
			}
			return 0;
		}
		
		private void Find(ITrain train)
		{
			startZone = 0;
			endZone = 0;

            WriteLine(lines[7]);
            while (true)
			{
				try
				{
					// TODO validation
					var readed = Read(2);
					if (readed.Length == 2)
					{						
						startZone = int.Parse(readed[0]);
						endZone = int.Parse(readed[1]);
						
						break;
					}
					else
					{						
						WriteLine(wrongInput);
						continue;
					}
				}
				catch (Exception ex)
				{
					WriteLines(new string[] { "error:", ex.Message });
				}
			}
			
			var result = train.FindAll(this.FindPassengerCarByRange);
			WriteCarriages(result);
		}
		
		int startZone, endZone;
		private bool FindPassengerCarByRange(ICarriage car)
		{
			if (car.GetCarriageType() == CarriageType.PassengerCar)
			{
				var count = ((PassengerCar)car).Passengers.Count;
				return count >= startZone && count <= endZone;
			}
			return false;
		}
	}
}
