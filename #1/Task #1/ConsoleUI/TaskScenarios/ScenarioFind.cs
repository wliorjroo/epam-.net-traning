﻿using System;
using RailwayTransport.Carriage;
using RailwayTransport.Carriage.PassengerCar;
using RailwayTransport.Train;

namespace ConsoleUI.TaskScenarios
{
	public class ScenarioFind : ScenarioCommand
	{
		private ITrain _train;
		private int startZone, endZone;
		private string FindHelp = "введите диапозон для поиска вагонов";
		
		public ScenarioFind(ITrain train)
		{
			_train = train;
		}
		
		public override void Execute()
		{
			Find(_train);
		}
		
		private void Find(ITrain train)
		{
			startZone = 0;
			endZone = 0;
			
			while(true)
			{
				WriteLine(FindHelp);
				try
				{
					// TODO validation
					var readed = Read(2);
					if (readed.Length == 2)
					{						
						startZone = int.Parse(readed[0]);
						endZone = int.Parse(readed[1]);
						
						break;
					}
					else
					{						
						WriteLine(wrongInput);
						continue;
					}
				}
				catch (Exception ex)
				{
					WriteLines(new string[] { "error:", ex.Message });
				}
			}
			
			var result = train.FindAll(this.FindPassengerCarByRange);
			WriteCarriages(result);
		}
		
		private bool FindPassengerCarByRange(ICarriage car)
		{
			if (car.GetCarriageType() == CarriageType.PassengerCar)
			{
				var count = ((PassengerCar)car).Passengers.Count;
				return count >= startZone && count <= endZone;
			}
			return false;
		}
	}
}
