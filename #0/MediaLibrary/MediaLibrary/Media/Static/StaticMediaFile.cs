﻿using System;

namespace MediaLibrary.Media.Static
{
	public abstract class StaticMediaFile : IStaticPlayable
	{
		protected string Name;
		protected string Path;

		public abstract void Play();

		public abstract void Stop();

		public bool Next()
		{
			return false;
		}

		public bool Previous()
		{
			return false;
		}

		public IStaticPlayable Find(Predicate<IStaticPlayable> match)
		{
			return match.Invoke(this) ? this : null;
		}
	}
}
