﻿using System;
using System.Collections.Generic;

namespace MediaLibrary.Media.Static
{
	public interface IStaticPlayable
	{
		void Play();
		void Stop();
		bool Next();
		bool Previous();
		IStaticPlayable Find(Predicate<IStaticPlayable> match);
	}
}
