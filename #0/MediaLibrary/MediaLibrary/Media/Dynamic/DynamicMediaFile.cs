﻿using System;
using System.IO;
using MediaLibrary.Media.Static;

namespace MediaLibrary.Media.Dynamic
{
	public abstract class DynamicMediaFile : StaticMediaFile, IDynamicPlayable
	{
		protected Stream Stream;

		public abstract void Pause();
	}
}
