﻿using System;
using MediaLibrary.Media.Static;

namespace MediaLibrary.Media.Dynamic
{
	public interface IDynamicPlayable : IStaticPlayable
	{
		void Pause();
	}
}
