﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MediaLibrary.Media.Dynamic;
using MediaLibrary.Media.Static;

namespace MediaLibrary.Library
{
	public class MediaList : ILibrary
	{
		private readonly IList<IStaticPlayable> _media;
		internal IList<IStaticPlayable> Media 
		{
			get {
				return _media; 
			}
		}
		
		private int _current;
		
		public IStaticPlayable Current { get; private set; }
		
		public string Name { get; private set; }
		
		private void Reset()
		{
			_current = -1;
			Current = null;
		}
		
		public MediaList()
		{
			Reset();
			_media = new List<IStaticPlayable>();
		}
		
		public MediaList(string name) : this()
		{
			Name = name;
		}

		public void Play()
		{
			while(Next())
			{
				Current.Play();
			}
			Reset();
		}

		public void Pause()
		{
			var dynamicPlayable = Current as IDynamicPlayable;
			if (dynamicPlayable != null)
			{
				dynamicPlayable.Pause();
			}
			else
			{
				Current.Stop();
			}
		}
		
		public void Stop()
		{
			if (Current != null)
				Current.Stop();
			Reset();
		}
		
		public bool Next()
		{
			if (Current != null && Current != this && Current.Next())
				return true;
			if (++_current >= _media.Count)
			{
				return false;
			}
			Current = _media[_current];
			return true;
		}
		
		public bool Previous()
		{
			if (Current != null && Current != this && Current.Previous())
				return true;
			if (--_current >= 0 && _current < _media.Count)
			{
				Current = _media[_current];
				return true;
			}
			return false;
		}

		public void Add(IStaticPlayable media)
		{
			if (media == this)
				throw new ArgumentException();
			_media.Add(media);
		}

		public void Delete(Predicate<IStaticPlayable> match)
		{
			if (match != null)
			{
				_media.Remove(_media.First(match.Invoke));
			}
		}

		public IStaticPlayable Find(Predicate<IStaticPlayable> match)
		{
			MediaList result = null;
			if (match != null)
			{
				result = new MediaList();
				foreach(var item in _media)
				{
					var searchResult = item.Find(match);
					if (searchResult != null)
						result.Add(item);
				}
			}
			return result;
		}
	}
}
