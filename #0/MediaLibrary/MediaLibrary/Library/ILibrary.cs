﻿using System;
using System.Collections.Generic;
using MediaLibrary.Media.Dynamic;
using MediaLibrary.Media.Static;

namespace MediaLibrary.Library
{
	public interface ILibrary : IDynamicPlayable
	{
		void Add(IStaticPlayable media);
		void Delete(Predicate<IStaticPlayable> match);
		new IStaticPlayable Find(Predicate<IStaticPlayable> match);
	}
}
