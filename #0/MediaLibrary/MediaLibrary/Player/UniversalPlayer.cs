﻿using System;
using MediaLibrary.Library;
using MediaLibrary.Media.Dynamic;
using MediaLibrary.Media.Static;

namespace MediaLibrary.Player
{
	public class UniversalPlayer : IPlayer
	{
		public bool IsCanPause { get; private set; }
		public bool IsList { get; private set; }
		public bool IsPlaying { get; private set; }
		
		private ILibrary _list;
		
		private void Initializer()
		{
			IsCanPause = false;
			IsList = false;
			IsPlaying = false;
		}
		
		public UniversalPlayer()
		{
			Initializer();
		}

		public void Load(IStaticPlayable media)
		{
			if (media != null)
			{
				if (media is ILibrary)
				{
					IsList = true;
					IsCanPause = true;
					_list = media as ILibrary;
				}
				else
				{
					_list = new MediaList();
					_list.Add(media);
					IsList = false;
					IsCanPause = media is IDynamicPlayable;
				}
				IsPlaying = false;
			}
			else
				Initializer();
		}

		public void Next()
		{
			if (IsList)
			{
				_list.Next();
			}
		}

		public void Previous()
		{
			if (IsList)
			{
				_list.Previous();
			}
		}
		
		public void Play()
		{
			if (_list == null || IsPlaying)
				return;
			
			IsPlaying = true;
			_list.Play();
		}
		
		public void Pause()
		{
			if (_list == null || !IsPlaying)
				return;
			
			IsPlaying = false;
			_list.Pause();
		}

		public void Stop()
		{
			if (_list == null)
				return;
			
			IsPlaying = false;
			_list.Stop();
		}
	}
}
