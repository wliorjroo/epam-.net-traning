﻿using System;
using MediaLibrary.Media.Static;

namespace MediaLibrary.Player
{
	public interface IPlayer
	{
		bool IsCanPause { get; }
		bool IsList { get; }
		void Load(IStaticPlayable media);
		void Next();
		void Previous();
		void Play();
		void Pause();
		void Stop();
	}
}
