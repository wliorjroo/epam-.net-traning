﻿using System;
using MediaLibrary.Library;
using MediaLibrary.Media.Dynamic;
using MediaLibrary.Media.Static;
using NSubstitute;
using NUnit.Framework;

namespace MediaLibrary.UnitTests.Library
{
	[TestFixture]
	public class MediaListTests
	{
		[Test]
		public void IsCanPlay_SingleIStaticPlayable_Playing()
		{
			var media = Substitute.For<IStaticPlayable>();
			var list = new MediaList();
			list.Add(media);
			
			list.Play();
			
			media.Received(1).Play();
		}
		
		[Test]
		[TestCase(5)]
		public void IsCanPlay_ListOfIStaticPlayable_Playing(int count)
		{
			var media = Substitute.For<IStaticPlayable>();
			var list = new MediaList();
			for (int i = 0; i < count; i++)
				list.Add(media);
			
			list.Play();
			
			media.Received(count).Play();
		}
		
		[Test]
		[TestCase(3)]
		public void IsCanPlay_MixedList_Playing(int count)
		{
			var staticMedia = Substitute.For<IStaticPlayable>();
			var dynamicMedia = Substitute.For<IDynamicPlayable>();
			var mediaList = Substitute.For<ILibrary>();
			var list = new MediaList();
			for (int i = 0; i < count; i++)
			{
				list.Add(staticMedia);
				list.Add(dynamicMedia);
				list.Add(mediaList);
			}
			
			list.Play();
			
			staticMedia.Received(count).Play();
			dynamicMedia.Received(count).Play();
			mediaList.Received(count).Play();
		}
		
		[Test]
		public void IsCanAdd_ListRefToItself_Throws()
		{
			var list = new MediaList();
			
			Assert.Catch<ArgumentException>(() => list.Add(list));
		}
	}
}
