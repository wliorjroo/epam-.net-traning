﻿using System;
using MediaLibrary.Library;
using MediaLibrary.Media.Dynamic;
using MediaLibrary.Media.Static;
using MediaLibrary.Player;
using NSubstitute;
using NUnit.Framework;

namespace MediaLibrary.UnitTests.Player
{
	[TestFixture]
	public class UniversalPlayerTests
	{		
		[Test]
		public void IsCanLoad_LoadMediaList_Loaded()
		{
			var media = Substitute.For<ILibrary>();
			var player = new UniversalPlayer();
			player.Load(media);
			
			Assert.True(player.IsCanPause && player.IsList);
		}
		
		[Test]
		public void IsCanPlay_PlayMediaList_Playing()
		{
			var mediaList = Substitute.For<ILibrary>();
			var player = new UniversalPlayer();
			player.Load(mediaList);
			player.Play();
			
			Assert.True(player.IsPlaying);
			mediaList.Received().Play();
		}
		
		[Ignore("need to move into integration tests")]
		[Test]
		public void IsCanStop_StopPlay_Stoped()
		{
			var media = Substitute.For<IStaticPlayable>();
			var player = new UniversalPlayer();
			player.Load(media);
			player.Play();
			player.Stop();
			
			Assert.False(player.IsPlaying);
			media.Received().Stop();
		}
		
		[Ignore("need to move into integration tests")]
		[Test]
		public void IsCanPause_PausePlay_Paused()
		{
			var media = Substitute.For<IDynamicPlayable>();
			var player = new UniversalPlayer();
			player.Load(media);
			player.Play();
			player.Pause();
			player.Play();
			
			Assert.True(player.IsPlaying);
			media.Received(2).Play();
		}
		
		[Ignore("need to move into integration tests")]
		[Test]
		public void IsCanResumePlay_ResumePlay_Resumed()
		{
			var media = Substitute.For<IDynamicPlayable>();
			var player = new UniversalPlayer();
			player.Load(media);
			player.Play();
			player.Pause();
			player.Play();
			
			Assert.True(player.IsPlaying);
			media.Received(2).Play();
		}
	}
}
